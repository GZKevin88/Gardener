<p align="center"><img src="https://images.gitee.com/uploads/images/2020/1204/145903_cea2bf9d_302533.png" height="80"/></p>

中文 | [English](README.en.md)

## 项目介绍

园丁是基于 .net 6开发的后台管理系统，系统前后台分离，api 是基于Furion 框架开发，前端是基于ant-design-blazor开发，系统使用技术或框架较新，喜欢的请点点star :kissing_heart: 。
## 演示地址
用户名：admin、admin1、admin2、admin3、admin4、admin5、admin6

密码：admin

请善待弱鸡 :two_hearts:[http://47.94.212.176:1000](http://47.94.212.176:1000)，初次加载较慢，客官先喝口水吧。请不要随便删除用户数据，演示服务不定期初始化，推荐docker运行进行查看。

 **docker 运行** 
```
docker pull huhangfei/gardener
docker run --name gardener -p 80:80 --restart=always -d huhangfei/gardener
```

## 项目特点
- 新：.Net6 、Blazor WebAssembly 、Furion ；全部新鲜。
- 简：功能简单实用

## 项目文档
[项目说明文档](https://gitee.com/hgflydream/Gardener/wikis)

## 贡献代码

感谢每一位为**园丁**贡献代码的朋友，欢迎大家提交 PR 或 Issue。

## 基情链接
👉 **[Furion](https://gitee.com/dotnetchina/Furion)**  
👉 **[ant-design-blazor](https://github.com/ant-design-blazor/ant-design-blazor)**

## 跟上组织

 **qq群**

<a target="_blank" href="https://qm.qq.com/cgi-bin/qm/qr?k=ILV3MBrcZtr4uUSsKa3njjnpBiUvT0xe&jump_from=webapi">
<img alt="点击加入" title="点击加入" src="https://images.gitee.com/uploads/images/2021/1101/112200_a6d329a3_302533.png" width="200px" height="200px"/>
</a>