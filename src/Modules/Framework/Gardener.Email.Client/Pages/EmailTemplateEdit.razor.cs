﻿// -----------------------------------------------------------------------------
// 园丁,是个很简单的管理系统
//  gitee:https://gitee.com/hgflydream/Gardener 
//  issues:https://gitee.com/hgflydream/Gardener/issues 
// -----------------------------------------------------------------------------

using Gardener.Client.Base;
using Gardener.Email.Dtos;
using System;

namespace Gardener.Email.Client.Pages
{
    public partial class EmailTemplateEdit : EditOperationDialogBase<EmailTemplateDto, Guid>
    {
    }
}
