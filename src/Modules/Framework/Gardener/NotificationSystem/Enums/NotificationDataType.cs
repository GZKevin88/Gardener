﻿// -----------------------------------------------------------------------------
// 园丁,是个很简单的管理系统
//  gitee:https://gitee.com/hgflydream/Gardener 
//  issues:https://gitee.com/hgflydream/Gardener/issues 
// -----------------------------------------------------------------------------

namespace Gardener.NotificationSystem.Enums
{
    /// <summary>
    /// 通知数据类型
    /// </summary>
    public enum NotificationDataType
    {
        /// <summary>
        /// 用户上线下线
        /// </summary>
        UserOnlineChange,
        /// <summary>
        /// 聊天
        /// </summary>
        Chat
    }
}
