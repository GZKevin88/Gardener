﻿// -----------------------------------------------------------------------------
// 园丁,是个很简单的管理系统
//  gitee:https://gitee.com/hgflydream/Gardener 
//  issues:https://gitee.com/hgflydream/Gardener/issues 
// -----------------------------------------------------------------------------

using Gardener.Client.Base;
using Gardener.UserCenter.Dtos;

namespace Gardener.UserCenter.Client.Pages.RoleView
{
    public partial class RoleEdit : EditOperationDialogBase<RoleDto, int>
    {
    }
}
