﻿// -----------------------------------------------------------------------------
// 园丁,是个很简单的管理系统
//  gitee:https://gitee.com/hgflydream/Gardener 
//  issues:https://gitee.com/hgflydream/Gardener/issues 
// -----------------------------------------------------------------------------

namespace Gardener.Enums
{
    /// <summary>
    /// 预设颜色
    /// </summary>
    public enum ClientAntPresetColor
    {
        Pink,
        Red,
        Yellow,
        Orange,
        Cyan,
        Green,
        Blue,
        Purple,
        GeekBlue,
        Magenta,
        Volcano,
        Gold,
        Lime
    }
}
